const express = require("express");
const router = express.Router();

const userController = require("../Controllers/userController.js");


// this route is for user registration
router.post("/register", userController.userRegistration);

// this route is for user authentication
router.post("/login", userController.userAuthentication);

// this route is for retrieving user
router.post("/details", userController.getProfile);














module.exports = router;