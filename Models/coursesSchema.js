const mongoose = require("mongoose");

const coursesSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Name of the course is required!"]
	},
	description: {
		type: String,
		required: [true, "Description of the course is required!"]
	},
	price: {
		type: Number,
		required: [true, "Price of the course is required!"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	enrollees: [
		{
			userId: {
				type: String,
				required: [true, "UserID is required!"]
			},
			enrolledOn: {
				type: Date,
				default: new Date()
			}
		}	
	]
})

module.exports = mongoose.model("Course", coursesSchema);