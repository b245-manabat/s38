const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors"); // by default our backend's CORS setting will prevent any application outside our Expressjs app to process the request. Using the cors package, it will allow us to manipulate this and control what applications may use our app
// allows our backend application to be available to our frontend application
// allow us to control the app's Cross Origin Resource Sharing

const userRoutes = require("./Routes/userRoutes.js");

const port = 3001;
const app = express();

mongoose.set('strictQuery',true);

mongoose.connect("mongodb+srv://admin:admin@batch245-manabat.mca9yeu.mongodb.net/batch245_Course_API_Manabat?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

let db = mongoose.connection;

db.on("error", console.error.bind(console, "Connection Error!"));
db.once("open", () => {console.log(`We are connected to the cloud!`)});

app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use(cors());



app.use("/user", userRoutes)




app.listen(port, () => console.log(`Server is running at port ${port}!`));