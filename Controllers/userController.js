const mongoose = require("mongoose");

const User = require("../Models/usersSchema.js");

const bcrypt = require("bcrypt");

const auth = require("../auth.js");


module.exports.userRegistration = (request, response) => {
	const input = request.body;
	User.findOne({email: input.email})
	.then(result => {
		if(result !== null) {
			return response.send("The email is already taken!")
		}
		else {
			let newUser = new User ({
				firstName: input.firstName,
				lastName: input.lastName,
				email: input.email,
				password: bcrypt.hashSync(input.password, 10),
				mobileNo: input.mobileNo
			})

			newUser.save()
			.then(save => {
				return response.send("You are now registered in our website!")
			})
			.catch(error => {
				return response.send(error)
			})
		}
	})
	.catch(error => {
		return response.send(error)
	})
}

// User Authentication
module.exports.userAuthentication = (request,response) => {
	let input = request.body;

	/*
	possible scenario in logging in
		1. email is not yet registered
		2/ email is registered but the password is wrong
	*/

	User.findOne({email: input.email})
	.then(result => {
		if(result === null) {
			return response.send("Email is not yet registered. Register first before logging in!")
		}
		else {
			// we have to verify if the password is correct
			// the "compareSync" method is used to compare a non encrypted password to the encrypted password.
			// it returns boolean valye, if match true valie will return otherwise false.
			const isPasswordCorrect = bcrypt.compareSync(input.password, result.password)
			if(isPasswordCorrect) {
				return response.send({auth: auth.createAccessToken(result)});
			}
			else {
				return response.send("Password is incorrect!")
			}
		}
	})
	.catch(error => {
		return response.send(error)
	})
}

// Retrieve User
module.exports.getProfile = (request,response) => {
	const input = request.body;

	User.findOne({_id: input.id})
	.then(result => {
		result.password = "*confidential*";
		return response.send(result)
	})
	.catch(error => {
		return response.send(error)
	})
}